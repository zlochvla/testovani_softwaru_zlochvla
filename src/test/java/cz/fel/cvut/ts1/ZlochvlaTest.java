package cz.fel.cvut.ts1;

import org.junit.jupiter.api.*;

class ZlochvlaTest {

    @Test
    void factorialTest() {
        Zlochvla z = new Zlochvla();
        Assertions.assertEquals(120, z.factorial(5));

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            z.factorial(-1);
        });

        Assertions.assertEquals(1, z.factorial(0));
    }
}
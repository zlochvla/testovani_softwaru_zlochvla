package cz.fel.cvut.ts1;

public class Zlochvla {
    public long factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n must be non-negative");
        }
        else if (n == 0) {
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }

    public static void main(String[] args) {
        Zlochvla z = new Zlochvla();
        System.out.println(z.factorial(5));
    }
}
